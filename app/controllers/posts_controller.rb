class PostsController < ApplicationController
  before_filter :authenticate_user!
  expose_decorated(:posts) { Post.where(archived: false) }
  expose_decorated(:post)

  def index
  end

  def new
  end

  def edit
  end

  def update
    if post.save
      render action: :index
    else
      render :new
    end
  end

  def destroy
    post = current_user.posts.find(params[:id]).destroy if post.present?

    render action: :index
  end

  def show
  end

  def mark_archived
    post = Post.find(params[:id])
    post.archived = true
    post.save

    render action: :index
  end

  def create
    if post.save
      redirect_to action: :index
    else
      render :new
    end
  end

end
