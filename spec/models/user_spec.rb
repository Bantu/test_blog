require 'spec_helper'

describe User do
  it { should have_field(:name) }
  it { should validate_presence_of(:name) }

  describe "#to_s" do
    it "returns name if nickname isn't set" do
      user = User.new(name: "john doe")
      user.name.to_s.should eq "john doe"
    end

    it "returns nickname if it's set" do
      user = User.new(name: "john doe", nickname: "rambo")
      user.name.to_s.should eq "john doe"
    end
  end

end
